import unittest
import welcome

class WelcomeTest(unittest.TestCase):
    def testWelcome(self):
        self.assertEqual(welcome.welcome(),'Welcome to CI/CD :) !!!!')

if __name__ == '__main__':
    unittest.main()
